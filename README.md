# Sensor Systems

## Thesis
Two semesters ago I took the class: Building a Sensor System, which awarded me the opportunity to learn how to interface between hardware/software. The class
was project based and gave students the opportunity to use Raspberry Pi's in conjunction with GPIO/circuit board interface. 

Since the class I've also used the class understanding to build a handful of personal projects on my own and for other classes.
## Abstract 
Inside of my Sensor Systems repo there's going to be a decent number of projects I've done that use hardware and interface the hardware with python software.
The purpose of this is to understand how to build/code systems that are used for experimentation and real life applications. There have been a number of real
world application I've used my experience in this class to build systems to solve other problems with.

Sensor systems have limitless applications in the real world, and are used in almost all applications of our daily life. 

## Data 
Along with each of the projects I've built, there's always a section included for data computation and explanation. It's important with sensor systems to be
able to evaluate the data given by the experimentation.

## Future Work
Because of my enjoyment with hardware/software applications and the projects I'm currently taking on, I'm always looking to get involved with more hardware 
applicable projects. 
